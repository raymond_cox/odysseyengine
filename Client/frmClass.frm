VERSION 5.00
Begin VB.Form frmClass 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "The Odyssey Online Classic [Editing Class]"
   ClientHeight    =   5715
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5955
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5715
   ScaleWidth      =   5955
   StartUpPosition =   2  'CenterScreen
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   8
      Left            =   1320
      Max             =   255
      TabIndex        =   33
      Top             =   4560
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   5
      Left            =   1320
      Max             =   255
      TabIndex        =   30
      Top             =   3480
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   4
      Left            =   1320
      Max             =   255
      TabIndex        =   28
      Top             =   3120
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   7
      Left            =   1320
      Max             =   255
      TabIndex        =   26
      Top             =   4200
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   6
      Left            =   1320
      Max             =   255
      TabIndex        =   24
      Top             =   3840
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   3
      Left            =   1320
      Max             =   255
      TabIndex        =   22
      Top             =   2760
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   2
      Left            =   1320
      Max             =   255
      TabIndex        =   20
      Top             =   2400
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   1
      Left            =   1320
      Max             =   255
      TabIndex        =   18
      Top             =   2040
      Width           =   3495
   End
   Begin VB.HScrollBar sclData 
      Height          =   255
      Index           =   0
      Left            =   1320
      Max             =   255
      TabIndex        =   16
      Top             =   1680
      Width           =   3495
   End
   Begin VB.CommandButton btnOk 
      Caption         =   "Save"
      Height          =   495
      Left            =   4320
      TabIndex        =   8
      Top             =   5040
      Width           =   1455
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Close"
      Height          =   495
      Left            =   2760
      TabIndex        =   7
      Top             =   5040
      Width           =   1455
   End
   Begin VB.HScrollBar sclSprite 
      Height          =   255
      Left            =   1320
      Max             =   977
      Min             =   1
      TabIndex        =   5
      Top             =   1200
      Value           =   1
      Width           =   3495
   End
   Begin VB.PictureBox picSprite 
      AutoRedraw      =   -1  'True
      Height          =   540
      Left            =   5280
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   3
      Top             =   600
      Width           =   540
   End
   Begin VB.TextBox txtName 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      MaxLength       =   15
      TabIndex        =   2
      Top             =   600
      Width           =   3855
   End
   Begin VB.Label lblSprite 
      Alignment       =   2  'Center
      Caption         =   "0"
      Height          =   255
      Left            =   4920
      TabIndex        =   36
      Top             =   1200
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Magic Defense:"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   35
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   8
      Left            =   4920
      TabIndex        =   34
      Top             =   4560
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stamina:"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   32
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   7
      Left            =   4920
      TabIndex        =   31
      Top             =   4200
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   6
      Left            =   4920
      TabIndex        =   29
      Top             =   3840
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   5
      Left            =   4920
      TabIndex        =   27
      Top             =   3480
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   4
      Left            =   4920
      TabIndex        =   25
      Top             =   3120
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   3
      Left            =   4920
      TabIndex        =   23
      Top             =   2760
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   2
      Left            =   4920
      TabIndex        =   21
      Top             =   2400
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   1
      Left            =   4920
      TabIndex        =   19
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label lblData 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Index           =   0
      Left            =   4920
      TabIndex        =   17
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Constitution:"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   15
      Top             =   3120
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Defense:"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   14
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Wisdom:"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   13
      Top             =   3840
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Concentration:"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   12
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Intelligence:"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   11
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Endurance:"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Attack:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Sprite:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Name:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label lblNumber 
      Alignment       =   2  'Center
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   4455
   End
   Begin VB.Label Label4 
      Caption         =   "Number:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnCancel_Click()
    Me.Hide
End Sub

Private Sub btnOk_Click()
    Dim St As String
    Dim A As Long
    
    St = Chr$(23) + Chr$(lblNumber) + DoubleChar$(sclSprite)
    
    For A = 0 To MaxStats
        St = St + Chr$(Val(sclData(A)))
    Next A
    
    St = St + txtName.Text

    SendSocket St
    Me.Hide
End Sub

Private Sub Form_Load()
    DrawToDC 0, 0, 32, 32, picSprite.hDC, DDSSprites, 96, (sclSprite - 1) * 32
    frmClass_Loaded = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmClass_Loaded = False
End Sub

Private Sub sclData_Change(index As Integer)
    lblData(index) = sclData(index)
End Sub

Private Sub sclData_Scroll(index As Integer)
    sclData_Change index
End Sub

Private Sub sclSprite_Change()
    DrawToDC 0, 0, 32, 32, picSprite.hDC, DDSSprites, 96, (sclSprite - 1) * 32
    picSprite.Refresh
    lblSprite.Caption = sclSprite
End Sub

Private Sub sclSprite_Scroll()
    sclSprite_Change
End Sub
